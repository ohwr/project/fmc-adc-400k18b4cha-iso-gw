--------------------------------------------------------------------------------
-- ALBA-CELLS (Computing Division, Electronics Section)
-- FMC ADC 400kSs/s core
-- http://www.ohwr.org/projects/fmc-adc-400k18b4cha
--------------------------------------------------------------------------------
--
-- unit name: AD79X0_CRTL (AD79X0_CRTL.vhd)
--
-- author: Xevi Serra-Gallifa (xserra@cells.es)
--
-- date: 07-07-2015
--
-- description: FMC ADC 400ks/s 18bits core. 
--
-- dependencies:
--
-- references:
--    
--
--------------------------------------------------------------------------------
-- GNU LESSER GENERAL PUBLIC LICENSE
--------------------------------------------------------------------------------
-- This source file is free software; you can redistribute it and/or modify it
-- under the terms of the GNU Lesser General Public License as published by the
-- Free Software Foundation; either version 2.1 of the License, or (at your
-- option) any later version. This source is distributed in the hope that it
-- will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
-- of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
-- See the GNU Lesser General Public License for more details. You should have
-- received a copy of the GNU Lesser General Public License along with this
-- source; if not, download it from http://www.gnu.org/licenses/lgpl-2.1.html
--------------------------------------------------------------------------------
-- last changes: see git log.
--------------------------------------------------------------------------------
-- TODO: - 
--------------------------------------------------------------------------------
-- Additional Comments: 
--------------------------------------------------------------------------------
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_arith.ALL;
use ieee.std_logic_unsigned.all;

library work;
use work.utils_pkg.all;

entity AD79X0_CRTL is
	Generic( 
		ADC_bits : 				INTEGER := 10; 	-- (AD7910 --> 10, AD7920 --> 12)										
		CLk_ticks_to_SPI :	INTEGER := 30										
	 );
	 Port ( reset : 			in  STD_LOGIC;
			  clk : 				in  STD_LOGIC;
           SCLK :				out STD_LOGIC;
			  SDATA : 			in  STD_LOGIC; --Registered input, to avoid meta-inestabilities
           nCS :	 			out STD_LOGIC;	
			  ADC_OUT : 		out STD_LOGIC_VECTOR (ADC_bits - 1 downto 0);
           DATA_READY : 	out STD_LOGIC
           );
end AD79X0_CRTL;

architecture Behavioral of AD79X0_CRTL is

constant half_CLk_ticks_int  : integer := CLk_ticks_to_SPI/2;
constant half_CLk_ticks      : STD_LOGIC_VECTOR := conv_std_logic_vector(half_CLk_ticks_int, log2_ceil(half_CLk_ticks_int));

--type state_type is (ADC_RST, idle, inConversion, wait_1clk_before_reading, reading);
--signal state, next_state : state_type := ADC_RST;


signal ADC_OUT_r : 		STD_LOGIC_VECTOR (ADC_bits - 1 downto 0);
signal counter_clk : 	STD_LOGIC_VECTOR (log2_ceil(half_CLk_ticks_int)-1 downto 0);
signal bits_red_cnt : 	STD_LOGIC_VECTOR (log2_ceil(ADC_bits + 3) downto 0);
signal  Din_r :			STD_LOGIC;
signal  nCS_s :			STD_LOGIC;
signal  SCLK_s :			STD_LOGIC;

begin
	
	--SDATA <= 'Z';
	nCS <= nCS_s;
	SCLK <= SCLK_s;
	
	assert CLk_ticks_to_SPI < 40
		report "nCS could be smaller than 10ns hight. Please modify the code to ensure the comunication"
		severity failure;
	
	process(clk)
	begin
		if rising_edge(clk) then 
			if reset = '1' then			
				DATA_READY <= '0';
				nCS_s <= '1';
			else
				if bits_red_cnt = ADC_bits + 4 then
					DATA_READY <= '1';
					nCS_s <= '1';
					ADC_OUT <= ADC_OUT_r; -- ┐┐necessary to register again?? Maybe not!
				else	
					DATA_READY <= '0';
					nCS_s <= '0';  -- 2 clk cycles = 16ns(>10ns) it is enough in this case
				end if;
			end if;
		end if;	
	end process;

	
	process(clk)
	begin
		if rising_edge(clk) then 
			if nCS_s = '1' then
				counter_clk <= (others => '0');
				bits_red_cnt <= (others => '0');
				SCLK_s <= '1';
			else
			
				Din_r <= SDATA; --to have registered the input and avoid meta-inestabilities
				if counter_clk = half_CLk_ticks then								
					if SCLK_s = '0' then
						SCLK_s <= '1';
					else
						SCLK_s <= '0';
						--if nCS_s = '0' then
							ADC_OUT_r <= ADC_OUT_r(ADC_OUT_r'high -1 downto ADC_OUT_r'low) & Din_r;
							bits_red_cnt <= bits_red_cnt + 1;
						--end if;
					end if;
					counter_clk <= (others => '0');
				else 
					counter_clk <= counter_clk + 1;
				end if;
			end if;
		end if;	
	end process;

	
end Behavioral;