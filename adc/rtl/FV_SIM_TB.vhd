----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    15:07:37 05/28/2015 
-- Design Name: 
-- Module Name:    CounterNb_TB - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--          To test the AD79X0_CRTL.vhd Floating voltage ADC
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;


library work;
use work.utils_pkg.all;


-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity SPI_SIM2_TB is
end SPI_SIM2_TB;

architecture Behavioral of SPI_SIM2_TB is
	
	component AD79X0_CRTL is
	Generic( 
		ADC_bits : 				INTEGER := 10; 	-- (AD7910 --> 10, AD7920 --> 12)										
		CLk_ticks_to_SPI :	INTEGER := 10										
	 );
	 Port ( reset : 			in  STD_LOGIC;
			  clk : 				in  STD_LOGIC;
           SCLK :				out STD_LOGIC;
			  SDATA : 			in  STD_LOGIC; --Registered input, to avoid meta-inestabilities
           nCS :	 			out STD_LOGIC;	
			  ADC_OUT : 		out STD_LOGIC_VECTOR (ADC_bits - 1 downto 0);
           DATA_READY : 	out STD_LOGIC
           );
end component AD79X0_CRTL;

constant adc_t4 : time := 40 ns;
constant adc_t7 : time := 10 ns;	

constant clk_period : time := 8 ns;
constant ADC_bits : integer := 10;

------------------------------SPI------------------------------
signal CLK : 					STD_LOGIC;
signal RESET : 				STD_LOGIC;
signal SPI_CLK : 				STD_LOGIC;
signal SDATA : 		 STD_LOGIC; --Registered input, to avoid meta-inestabilities
signal nCS :	 		 STD_LOGIC;	
signal ADC_OUT : 		 STD_LOGIC_VECTOR (ADC_bits - 1 downto 0);
signal DATA_READY : 	 STD_LOGIC;
------------------------------/SPI------------------------------
constant data : STD_LOGIC_VECTOR := "0000101101110111111";
signal counter: STD_LOGIC_VECTOR(log2_ceil(data'length) downto 0);


begin


 COMP0 : AD79X0_CRTL 
--	Generic map( 
--		ADC_bits  => ADC_bits, 	-- (AD7910 --> 10, AD7920 --> 12)										
--		CLk_ticks_to_SPI => 10										
--	 )
	 Port map ( reset => RESET,
			  clk => CLK,
           SCLK => SPI_CLK,
			  SDATA => SDATA,
           nCS => nCS,
			  ADC_OUT => ADC_OUT,
           DATA_READY => DATA_READY
           );

	
	-- CLOCK PROCESS
	process
	begin
		clock_loop: loop
			CLK <= '0';
			wait for clk_period * 0.5;
			CLK <= '1';
			wait for clk_period * 0.5;
		end loop clock_loop;
	end process;
	
	process(SPI_CLK,nCS)
	begin
		if nCS = '1' then
			counter<= (others => '0');
		elsif falling_edge(SPI_CLK) then
			counter<= counter + 1;
		end if;	
	end process;
	
	process
	begin
		wait on SPI_CLK;
		if SPI_CLK = '0' then
			wait for adc_t7;
			SDATA <= 'U';
			wait for adc_t4 - adc_t7;
			SDATA <= data(conv_integer(counter));
		end if;	
	end process;
	
	-- Behavioral Simulation process;
	process
	begin
		
		--Initialization
		RESET <='0';
			  		
		--Reset
		wait for 100 ns;
		RESET <= '1';
		wait for 50 ns;
		RESET <= '0';
		
		
		wait for 200000 ns;


	end process;
	


end Behavioral;

