--------------------------------------------------------------------------------
-- CERN (BE-CO-HT)
-- FMC ADC mezzanine
-- http://www.ohwr.org/projects/fmc-adc-100m14b4cha
--------------------------------------------------------------------------------
--
-- unit name: fmc_adc_mezzanine (fmc_adc_mezzanine.vhd)
--
-- author: Matthieu Cattin (matthieu.cattin@cern.ch)
-- modified: Xavier Serra  (xserra@cells.es)
--
-- date: 07-05-2013
--
-- description: The FMC ADC mezzanine is wrapper around the fmc-adc-100ms core and
--              the other wishbone slaves connected to a FMC ADC mezzanine.
--
-- dependencies:
--
-- references:
--
--------------------------------------------------------------------------------
-- GNU LESSER GENERAL PUBLIC LICENSE
--------------------------------------------------------------------------------
-- This source file is free software; you can redistribute it and/or modify it
-- under the terms of the GNU Lesser General Public License as published by the
-- Free Software Foundation; either version 2.1 of the License, or (at your
-- option) any later version. This source is distributed in the hope that it
-- will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
-- of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
-- See the GNU Lesser General Public License for more details. You should have
-- received a copy of the GNU Lesser General Public License along with this
-- source; if not, download it from http://www.gnu.org/licenses/lgpl-2.1.html
--------------------------------------------------------------------------------
-- last changes: see git log.
--------------------------------------------------------------------------------
-- TODO: - 
--------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;


library work;
use work.wishbone_pkg.all;



entity fmc_adc_mezzanine is
  generic(
    g_carrier_type       : string  := "SPEC"
    );
  port (
    -- Clock, reset
    sys_clk_i   : in std_logic;
    sys_rst_n_i : in std_logic;
	 adc_clk_i		 : in std_logic;
    -- CSR wishbone interface
    wb_csr_adr_i   : in  std_logic_vector(31 downto 0);
    wb_csr_dat_i   : in  std_logic_vector(31 downto 0);
    wb_csr_dat_o   : out std_logic_vector(31 downto 0);
    wb_csr_cyc_i   : in  std_logic;
    wb_csr_sel_i   : in  std_logic_vector(3 downto 0);
    wb_csr_stb_i   : in  std_logic;
    wb_csr_we_i    : in  std_logic;
    wb_csr_ack_o   : out std_logic;
    wb_csr_stall_o : out std_logic;


    -- FMC interface
		LA_P    : inout std_logic_vector(33 downto 0);
		LA_N    : inout std_logic_vector(33 downto 0);

      sys_scl_b : inout std_logic;      -- Mezzanine system I2C clock (EEPROM)
      sys_sda_b : inout std_logic       -- Mezzanine system I2C data (EEPROM)

    );
end fmc_adc_mezzanine;


architecture rtl of fmc_adc_mezzanine is

  ------------------------------------------------------------------------------
  -- Components declaration
  ------------------------------------------------------------------------------
component FV_CONTROL is
	 Port ( 
		-- Clock, reset
		sys_clk_i   : in std_logic;
		sys_rst_n_i : in std_logic;


		-- CSR wishbone interface
		wb_csr_adr_i : in  std_logic_vector(0 downto 0);
		wb_csr_dat_i : in  std_logic_vector(31 downto 0);
		wb_csr_dat_o : out std_logic_vector(31 downto 0);
		wb_csr_cyc_i : in  std_logic;
		wb_csr_sel_i : in  std_logic_vector(3 downto 0);
		wb_csr_stb_i : in  std_logic;
		wb_csr_we_i  : in  std_logic;
		wb_csr_ack_o : out std_logic;


		-- FMC interface	 
		SCLK 				: out STD_LOGIC;
		SDATA 			: in  STD_LOGIC; --Registered input, to avoid meta-inestabilities
		nCS 				: out STD_LOGIC;	
		HV_Led 			: out STD_LOGIC;
		SP1_Led 			: out STD_LOGIC;	
		HV_Relay 		: out STD_LOGIC
      );
end  component FV_CONTROL;
  
component FMC_ADC_400k_core is
  generic(
    g_carrier_type       : string  := "SPEC"
    );
  port (
    -- Clock, reset 
    sys_clk_i   		: in std_logic;
    sys_rst_n_i 		: in std_logic;
	 
	 adc_clk_i 			: in std_logic;

    -- CSR wishbone interface
    wb_csr_adr_i 		: in  std_logic_vector(3 downto 0);
    wb_csr_dat_i 		: in  std_logic_vector(31 downto 0);
    wb_csr_dat_o 		: out std_logic_vector(31 downto 0);
    wb_csr_cyc_i 		: in  std_logic;
    wb_csr_sel_i 		: in  std_logic_vector(3 downto 0);
    wb_csr_stb_i 		: in  std_logic;
    wb_csr_we_i  		: in  std_logic;
    wb_csr_ack_o 		: out std_logic;


    -- FMC interface	 
	 adc_RESET			: out std_logic;
	 adc_nCS				: out std_logic;
	 adc_Data_OUT		: in STD_LOGIC_VECTOR ( 1 downto 0);
	 adc_SCLK			: out std_logic;
	 adc_ConvStart		: out STD_LOGIC_VECTOR ( 1 downto 0);
	 adc_BUSY			: in std_logic;
	 adc_OverSam		: out std_logic_vector(2 downto 0);
	 adc_Range			: out std_logic
	 
	 
    );
end component FMC_ADC_400k_core;



  ------------------------------------------------------------------------------
  -- SDB crossbar constants declaration
  --
  -- WARNING: All address in sdb and crossbar are BYTE addresses!
  ------------------------------------------------------------------------------

  -- Number of master port(s) on the wishbone crossbar
  constant c_NUM_WB_MASTERS 			: integer := 3; 

  -- Number of slave port(s) on the wishbone crossbar
  constant c_NUM_WB_SLAVES 			: integer := 1;

  -- Wishbone master(s)
  constant c_WB_MASTER 					: integer := 0;

  -- Wishbone slave(s)
  constant c_WB_SLAVE_FMC_SYS_I2C 	: integer := 0;  -- Mezzanine system I2C interface (EEPROM)

  constant c_WB_SLAVE_FMC_ADC     	: integer := 1;  -- Mezzanine ADC core
  constant c_WB_SLAVE_FMC_FV_CTRL 	: integer := 2;  -- Mezzanine ADC core

  -- Devices sdb description
  constant c_wb_adc_csr_sdb : t_sdb_device := (
    abi_class     => x"0000",              -- undocumented device
    abi_ver_major => x"01",
    abi_ver_minor => x"01",
    wbd_endian    => c_sdb_endian_big,
    wbd_width     => x"4",                 -- 32-bit port granularity
    sdb_component => (
      addr_first  => x"0000000000000000",
      addr_last   => x"000000000000002b",
      product     => (
        vendor_id => x"000000000000a1ba",  -- ALBA
        device_id => x"00000608",
        version   => x"00000001",
        date      => x"20150707",
        name      => "WB-FMC-ADC-Core    ")));



  constant c_wb_fv_ctrl_csr_sdb : t_sdb_device := (
    abi_class     => x"0000",              	-- undocumented device
    abi_ver_major => x"01",
    abi_ver_minor => x"01",
    wbd_endian    => c_sdb_endian_big,
    wbd_width     => x"4",                 	-- 32-bit port granularity
    sdb_component => (
      addr_first  => x"0000000000000000",
      addr_last   => x"0000000000000007",
      product     => (
        vendor_id => x"000000000000a1ba",  	-- ALBA
        device_id => x"7565c07b",    			-- echo "WB-FMC-FV-CONTROL  " | md5sum | cut -c1-8
        version   => x"00000001",
        date      => x"20150722",
        name      => "WB-FMC-FV-CONTROL  ")));


  -- sdb header address
  constant c_SDB_ADDRESS : t_wishbone_address := x"00000000";

  -- Wishbone crossbar layout

  constant c_INTERCONNECT_LAYOUT : t_sdb_record_array(c_NUM_WB_MASTERS-1 downto 0) :=
    (
      0 => f_sdb_embed_device(c_xwb_i2c_master_sdb, x"00001000"),
      1 => f_sdb_embed_device(c_wb_adc_csr_sdb, x"00001100"),
		2 => f_sdb_embed_device(c_wb_fv_ctrl_csr_sdb, x"00001200")
      );

  ------------------------------------------------------------------------------
  -- Signals declaration
  ------------------------------------------------------------------------------

  -- Wishbone buse(s) from crossbar master port(s)
  signal cnx_master_out 	: t_wishbone_master_out_array(c_NUM_WB_MASTERS-1 downto 0);
  signal cnx_master_in  	: t_wishbone_master_in_array(c_NUM_WB_MASTERS-1 downto 0);

  -- Wishbone buse(s) to crossbar slave port(s)
  signal cnx_slave_out 		: t_wishbone_slave_out_array(c_NUM_WB_SLAVES-1 downto 0);
  signal cnx_slave_in  		: t_wishbone_slave_in_array(c_NUM_WB_SLAVES-1 downto 0);

  -- Mezzanine system I2C for EEPROM
  signal sys_scl_in   		: std_logic_vector(0 downto 0); --std_logic;
  signal sys_scl_out  		: std_logic_vector(0 downto 0); --std_logic;
  signal sys_scl_oe_n 		: std_logic_vector(0 downto 0); --std_logic;
  signal sys_sda_in   		: std_logic_vector(0 downto 0); --std_logic;
  signal sys_sda_out  		: std_logic_vector(0 downto 0); --std_logic;
  signal sys_sda_oe_n 		: std_logic_vector(0 downto 0); --std_logic;

  -- Mezzanine ADC
  signal adc_ConvStart		: STD_LOGIC_VECTOR ( 1 downto 0);
  signal adc_busy				: std_logic;
  signal adc_oversam			: std_logic_vector(2 downto 0);
  signal adc_range			: std_logic;
  



begin

  ------------------------------------------------------------------------------
  -- CSR wishbone crossbar
  ------------------------------------------------------------------------------
  cmp_sdb_crossbar : xwb_sdb_crossbar
    generic map (
      g_num_masters => c_NUM_WB_SLAVES,
      g_num_slaves  => c_NUM_WB_MASTERS,
      g_registered  => true,
      g_wraparound  => true,
      g_layout      => c_INTERCONNECT_LAYOUT,
      g_sdb_addr    => c_SDB_ADDRESS)
    port map (
      clk_sys_i => sys_clk_i,
      rst_n_i   => sys_rst_n_i,
      slave_i   => cnx_slave_in,
      slave_o   => cnx_slave_out,
      master_i  => cnx_master_in,
      master_o  => cnx_master_out);

  -- Connect crossbar slave port to entity port
  cnx_slave_in(c_WB_MASTER).adr <= wb_csr_adr_i;
  cnx_slave_in(c_WB_MASTER).dat <= wb_csr_dat_i;
  cnx_slave_in(c_WB_MASTER).sel <= wb_csr_sel_i;
  cnx_slave_in(c_WB_MASTER).stb <= wb_csr_stb_i;
  cnx_slave_in(c_WB_MASTER).we  <= wb_csr_we_i;
  cnx_slave_in(c_WB_MASTER).cyc <= wb_csr_cyc_i;

  wb_csr_dat_o   <= cnx_slave_out(c_WB_MASTER).dat;
  wb_csr_ack_o   <= cnx_slave_out(c_WB_MASTER).ack;
  wb_csr_stall_o <= cnx_slave_out(c_WB_MASTER).stall;

  ------------------------------------------------------------------------------
  -- Mezzanine system managment I2C master
  --    Access to mezzanine EEPROM
  ------------------------------------------------------------------------------
  cmp_fmc_sys_i2c : xwb_i2c_master
    generic map(
      g_interface_mode      => CLASSIC,
      g_address_granularity => BYTE
      )
    port map (
      clk_sys_i => sys_clk_i,
      rst_n_i   => sys_rst_n_i,

      slave_i => cnx_master_out(c_WB_SLAVE_FMC_SYS_I2C),
      slave_o => cnx_master_in(c_WB_SLAVE_FMC_SYS_I2C),
      desc_o  => open,

      scl_pad_i    => sys_scl_in,
      scl_pad_o    => sys_scl_out,
      scl_padoen_o => sys_scl_oe_n,
      sda_pad_i    => sys_sda_in,
      sda_pad_o    => sys_sda_out,
      sda_padoen_o => sys_sda_oe_n
      );

  -- Tri-state buffer for SDA and SCL
  sys_scl_b  <= sys_scl_out(0) when sys_scl_oe_n(0) = '0' else 'Z';
  sys_scl_in(0) <= sys_scl_b;

  sys_sda_b  <= sys_sda_out(0) when sys_sda_oe_n(0) = '0' else 'Z';
  sys_sda_in(0) <= sys_sda_b;


  ------------------------------------------------------------------------------
  -- FV control
  --    Control the floating ground voltage
  ------------------------------------------------------------------------------
  
FV_CONTROL_inst : FV_CONTROL
	 Port map( 
		-- Clock, reset
		sys_clk_i   	=> sys_clk_i,
      sys_rst_n_i 	=> sys_rst_n_i,


		-- CSR wishbone interface
      wb_csr_adr_i 	=> cnx_master_out(c_WB_SLAVE_FMC_FV_CTRL).adr(2 downto 2),  -- cnx_master_out.adr is byte address
      wb_csr_dat_i 	=> cnx_master_out(c_WB_SLAVE_FMC_FV_CTRL).dat,
      wb_csr_dat_o 	=> cnx_master_in(c_WB_SLAVE_FMC_FV_CTRL).dat,
      wb_csr_cyc_i 	=> cnx_master_out(c_WB_SLAVE_FMC_FV_CTRL).cyc,
      wb_csr_sel_i 	=> cnx_master_out(c_WB_SLAVE_FMC_FV_CTRL).sel,
      wb_csr_stb_i 	=> cnx_master_out(c_WB_SLAVE_FMC_FV_CTRL).stb,
      wb_csr_we_i  	=> cnx_master_out(c_WB_SLAVE_FMC_FV_CTRL).we,
      wb_csr_ack_o 	=> cnx_master_in(c_WB_SLAVE_FMC_FV_CTRL).ack,


		-- FMC interface	 
		SCLK				=> LA_P(28),
		SDATA				=> LA_N(24),
		nCS				=> LA_P(24),
		HV_Led			=> LA_N(28),
		SP1_Led			=> LA_P(30),
		HV_Relay 		=> LA_N(12)
      );
		
		LA_P(12)<= 'Z';
  ------------------------------------------------------------------------------
  -- ADC core
  --    ADC core control and status
  ------------------------------------------------------------------------------
  
	LA_P(2)<= adc_OverSam(0);
	LA_N(2)<= adc_OverSam(1);
	LA_P(4)<= adc_OverSam(2);
 
	LA_N(11)<= adc_ConvStart(1);
	LA_P(11)<= adc_ConvStart(0);
  
  cmp_FMC_ADC_400k_core : FMC_ADC_400k_core
    generic map (
      g_carrier_type       => g_carrier_type
      )
    port map(
      sys_clk_i   => sys_clk_i,
      sys_rst_n_i => sys_rst_n_i,
		adc_clk_i	=> adc_clk_i,

      wb_csr_adr_i => cnx_master_out(c_WB_SLAVE_FMC_ADC).adr(5 downto 2),  -- cnx_master_out.adr is byte address
      wb_csr_dat_i => cnx_master_out(c_WB_SLAVE_FMC_ADC).dat,
      wb_csr_dat_o => cnx_master_in(c_WB_SLAVE_FMC_ADC).dat,
      wb_csr_cyc_i => cnx_master_out(c_WB_SLAVE_FMC_ADC).cyc,
      wb_csr_sel_i => cnx_master_out(c_WB_SLAVE_FMC_ADC).sel,
      wb_csr_stb_i => cnx_master_out(c_WB_SLAVE_FMC_ADC).stb,
      wb_csr_we_i  => cnx_master_out(c_WB_SLAVE_FMC_ADC).we,
      wb_csr_ack_o => cnx_master_in(c_WB_SLAVE_FMC_ADC).ack,

     -- FMC interface	 

	 adc_RESET			=> LA_P(7),
	 adc_nCS				=> LA_N(7),
	 adc_Data_OUT		=> LA_N(19) & LA_P(19),
	 adc_SCLK			=>  LA_P(15),
	 adc_ConvStart		=> adc_ConvStart,
	 adc_BUSY			=> LA_N(15),
	 adc_OverSam		=> adc_OverSam,
	 adc_Range			=> adc_Range

      );
		


  -- Unused wishbone signals
  cnx_master_in(c_WB_SLAVE_FMC_ADC).err   <= '0';
  cnx_master_in(c_WB_SLAVE_FMC_ADC).rty   <= '0';
  cnx_master_in(c_WB_SLAVE_FMC_ADC).stall <= '0';
  cnx_master_in(c_WB_SLAVE_FMC_ADC).int   <= '0';

end rtl;
