--------------------------------------------------------------------------------
-- ALBA-CELLS (Computing Division, Electronics Section)
-- FMC ADC 400kSs/s core
-- http://www.ohwr.org/projects/fmc-adc-400k18b4cha
--------------------------------------------------------------------------------
--
-- unit name: fmc_adc_18b_400ks_pkg (fmc_adc_18b_400ks_pkg.vhd)
--
-- author: Xevi Serra-Gallifa (xserra@cells.es)
--
-- date: 07-07-2015
--
-- description: FMC FV ADC utils package. 
--
-- dependencies:
--
-- references:
--    
--
--------------------------------------------------------------------------------
-- GNU LESSER GENERAL PUBLIC LICENSE
--------------------------------------------------------------------------------
-- This source file is free software; you can redistribute it and/or modify it
-- under the terms of the GNU Lesser General Public License as published by the
-- Free Software Foundation; either version 2.1 of the License, or (at your
-- option) any later version. This source is distributed in the hope that it
-- will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
-- of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
-- See the GNU Lesser General Public License for more details. You should have
-- received a copy of the GNU Lesser General Public License along with this
-- source; if not, download it from http://www.gnu.org/licenses/lgpl-2.1.html
--------------------------------------------------------------------------------
-- last changes: see git log.
--------------------------------------------------------------------------------
-- TODO: - 
--------------------------------------------------------------------------------
-- Additional Comments: 
--------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;


--=============================================================================
--! Package declaration for fmc_adc_18b_400ks_pkg
--=============================================================================
package fmc_adc_18b_400ks_pkg is
	-- Constants
	Constant MaxVolt : integer := 1000;
	Constant MinVolt : integer := -1000;
	
	-- Functions
	function volt_to_adc(voltage : integer) return STD_LOGIC_VECTOR;
	
end fmc_adc_18b_400ks_pkg;


--=============================================================================
--! Body declaration for utils_pkg
--=============================================================================
package body fmc_adc_18b_400ks_pkg is

--*****************************************************************************
--! Function : Returns the STD_LOGIC_VECTOR equivalent to a given voltage
--*****************************************************************************
  function volt_to_adc(voltage : integer) return STD_LOGIC_VECTOR is
  constant ADC_bits : integer := 10;
  variable temp3 : SIGNED(ADC_bits - 1 downto 0);
  variable temp4 : STD_LOGIC_VECTOR(ADC_bits - 1 downto 0);
  begin
--    return conv_std_logic_vector(1024/1001*(voltage/2.5 + 500), ADC_bits);
		temp3:= to_signed((409*voltage + 511489)/1000, ADC_bits);
		temp4(ADC_bits - 1 downto 0):= std_logic_vector(temp3);
		return temp4;
  end;
end fmc_adc_18b_400ks_pkg;
