----------------------------------------------------------------------------------
-- Company: 	ALBA-CELLS (Barcelona)
-- Engineer:   Xevi Serra-Gallifa
-- 
-- Create Date:    09:15:19 04/19/2013 
-- Design Name: 
-- Module Name:    AD7608_CRTL - Behavioral 
-- Project Name:    ALBA Electrometer 2 (Em#)
-- Target Devices:  Version 2.0 modified to be used in FMC_ADC_ISO_400k_18b_4ch V2.0 connected to SPECS v4 board
-- Tool versions: ISE 14.7
-- Description:  
-- 2.0 ( modification for big delays of isoltors)
-- 
-- The signal Trig con not be set '1' if convst(0) is '1'
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Revision 1.0  - Developed to be use with spartan3 demo board (xc3s500e-fg320-4) with ADC7608 demo board. Release for BL22 tests 11/19/2013
-- Revision 2.0  - Modification to effort the big delays of digital isolator of FMC_ADC_ISO_400k_18b_4ch board.
-- Additional Comments: 
--
-- Actual delays have to be checked with ociloscope the teoric ones works perfect, remember that data is kept with CLK-cycle delay.
--	#Remember to put the following in the constraints file to keep data integrity
--	INST "LA_N(19)" TNM = "DATA_IN"; #LA_N(19) & LA_P(19)
--	INST "LA_P(19)" TNM = "DATA_IN";
--	INST "LA_P(15)" TNM = "ADC_SCLK";
--	INST "LA_N(7)"  TNM = "ADC_nCS";
--	#rising edge coment
--	TIMEGRP "DATA_IN" OFFSET = IN 30 ns VALID 30 ns BEFORE "adc_clk_in" TIMEGRP "ADC_SCLK" RISING;
--	TIMEGRP "DATA_IN" OFFSET = IN 30 ns VALID 30 ns BEFORE "adc_clk_in" TIMEGRP "ADC_nCS" FALLING;
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_arith.ALL;
use ieee.std_logic_unsigned.all;


entity AD7608_CRTL is
    Generic( 
		CLK_To_5us : INTEGER := 100; 	-- Tconversion/ Tclk = 5us/50ns = 100clock cycles.	(20MHz clk => 50ns)											
		DISABLE_CLK : BOOLEAN := TRUE -- Disables Clock when is not needed. It should reduce noise.
												
												
	 );
	 
	 Port ( n_reset : 		in  STD_LOGIC;
			  clk : 				in  STD_LOGIC;
           SCLK :				out STD_LOGIC;
			  Din : 				in  STD_LOGIC_VECTOR (1 downto 0);  --Registered input, to avoid meta-inestabilities
           Trig : 			in  STD_LOGIC;								--Registered input, to avoid meta-inestabilities
			  n_DblRateInput: in  STD_LOGIC;								--Recomended to do a reset after a change
			  ReadInConv:		in  STD_LOGIC; --  becareful when 1 with n_DblRateInput   --Recomended to do a reset after a change
			  ADC_BUSY: 		in  STD_LOGIC;
			  ADC_RESET : 		out STD_LOGIC;
			  nCS : 				out STD_LOGIC;
			  CONVST: 			out STD_LOGIC_VECTOR ( 1 downto 0);
           ADC_OUT_A : 		out STD_LOGIC_VECTOR (17 downto 0);
			  ADC_OUT_B : 		out STD_LOGIC_VECTOR (17 downto 0);
           ADC_num : 		out STD_LOGIC_VECTOR (1 downto 0);
           DATA_READY : 	out STD_LOGIC;
			  FSMstate :      out STD_LOGIC_VECTOR ( 2 downto 0)
           );
end AD7608_CRTL;

architecture Behavioral of AD7608_CRTL is


	type state_type is (ADC_RST, idle, inConversion, wait_1clk_before_reading, reading);
	signal state, next_state : state_type := ADC_RST;

	signal counter						: STD_LOGIC_VECTOR (7 downto 0):=
											  conv_std_logic_vector(CLK_To_5us + 1, 8);
			
	signal en_counter					: STD_LOGIC :='0';
	signal bit_count					: STD_LOGIC_VECTOR(4 downto 0);

	signal DATA_READY_i				: STD_LOGIC;
	signal ADC_num_r					: STD_LOGIC_VECTOR (ADC_num'range);

	signal reading_end				: STD_LOGIC;
	
	signal nCS_i						: STD_LOGIC;

	signal Din_r						: STD_LOGIC_VECTOR (1 downto 0);


	signal ADC_OUT_A_t				: STD_LOGIC_VECTOR (17 downto 0);
	signal ADC_OUT_B_t				: STD_LOGIC_VECTOR (17 downto 0);

	signal FSMstate_s 				: STD_LOGIC_VECTOR ( 2 downto 0);
	signal n_DblRateInput_r 		: STD_LOGIC_VECTOR ( 1 downto 0);
	signal n_reset_r 					: STD_LOGIC_VECTOR ( 1 downto 0);
	signal Trig_r						: STD_LOGIC_VECTOR ( 1 downto 0);
	signal ADC_BUSY_r					: STD_LOGIC;


begin
	
	
	nCS<= nCS_i;
	
DIS_CLK:	if DISABLE_CLK 
		generate 
			SCLK<= clk when nCS_i= '0' else '1'; 
		end generate DIS_CLK;
		 
EN_CLK:	if not DISABLE_CLK 
		generate 
			SCLK<= clk; 
		end generate EN_CLK;

	
	
procX:process(clk)
	begin
		if rising_edge(clk) then
			n_reset_r(0) <= n_reset;
			n_reset_r(1) <= n_reset_r(0);
			
			Trig_r(0)<= trig;
			Trig_r(1)<= Trig_r(0);
			
			n_DblRateInput_r(0) <= n_DblRateInput;
			n_DblRateInput_r(1) <= n_DblRateInput_r(0);
			
			if n_reset_r(1) = '0' then
				state <= ADC_RST;
				Trig_r <= (others => '0');
			else
				state <= next_state;
				ADC_BUSY_r <= ADC_BUSY;
				FSMstate <= FSMstate_s;
			end if;
		end if;
	end process;
	
	process(state, ADC_BUSY_r, DATA_READY_i, ADC_num_r)
	begin
		
		case state is
		when ADC_RST =>
			-- typicaly 1 CLK is more than the 25ns necessary for a ADC reset
			next_state <= idle;
			ADC_RESET <= '1';
			nCS_i <= '1';
			FSMstate_s <="000";
			
		when idle =>
			ADC_RESET<= '0';
			nCS_i<= '1';
			if ADC_BUSY_r = '1' then 
				next_state <= inConversion;
			else
				next_state <= idle;
			end if;
			FSMstate_s <="001";
			
		when inConversion =>
			ADC_RESET<= '0';			
			if ADC_BUSY_r = '0' then 
				next_state <= wait_1clk_before_reading;
				nCS_i<= '0';
			else
				next_state <= inConversion;
				nCS_i<= '1';
			end if;
			FSMstate_s <="010";
			
		when wait_1clk_before_reading =>  -- This is to compensate signal delay
			ADC_RESET<= '0';
			nCS_i<= '0';
			next_state <= reading;
			FSMstate_s <="011";
			
		when reading =>			
			ADC_RESET<= '0';
			nCS_i<= '0';
			if ADC_num_r = "00" and DATA_READY_i= '1' then 
				next_state <= idle;
			else
				next_state <= reading;
			end if;
			FSMstate_s <="100";
			
		when others =>
			next_state <= ADC_RST;
			FSMstate_s <="111";
		end case;
	end process;
	
	
	process(clk)
	begin
		if rising_edge(clk) then 
			Din_r<= Din; --to have registered the input and avoid meta-inestabilities
			if state = reading then
				ADC_OUT_A_t(conv_integer(bit_count))<= Din_r(0);
				ADC_OUT_B_t(conv_integer(bit_count))<= Din_r(1);			
				if bit_count = 0 then
					bit_count<= conv_std_logic_vector(17, bit_count'LENGTH);
					ADC_OUT_A<= ADC_OUT_A_t(17 downto 1) & Din_r(0); --to have registered the output and avoid meta-inestabilities
					ADC_OUT_B<= ADC_OUT_B_t(17 downto 1) & Din_r(1); --to have registered the output and avoid meta-inestabilities
					ADC_num<= ADC_num_r;
					ADC_num_r<= ADC_num_r + 1;
					DATA_READY_i <= '1';
				else
					DATA_READY_i <= '0';
					bit_count<= bit_count - 1;
				end if;
				
			else
				bit_count<= conv_std_logic_vector(17, bit_count'LENGTH);
				DATA_READY_i <= '0';
				ADC_num_r<= "00";
			end if;
			DATA_READY<= DATA_READY_i;  --to have registered the output and avoid meta-inestabilities
			
		end if;	
		
	end process;
	--DATA_READY<= DATA_READY_i;  -- It works, but I have doubts
	--ADC_OUT_A<= ADC_OUT_A_t;		-- It works, but I have doubts
	--ADC_OUT_B<= ADC_OUT_B_t;		-- It works, but I have doubts
	
	
	process(clk)  --, trig, ADC_BUSY_r
	begin
		if rising_edge(clk) then
			if en_counter = '0' then
				if Trig_r(1) = '1' then
					counter <= (Others => '0');
					en_counter<= '1';
					CONVST(0) <= '1';						--CONVSTA
					CONVST(1) <=  n_DblRateInput_r(1); 	--CONVSTB
				end if;
			else
				
				counter<= counter + 1;
				if counter = conv_std_logic_vector(CLK_To_5us/2, counter'LENGTH)then
					-- Tconversion/ Tclk = 5us/60ns = 83clock cycles 
					CONVST(0) <= '1';						--CONVSTA
					CONVST(1) <= '1'; 
										
				elsif counter >= conv_std_logic_vector(CLK_To_5us-1, counter'LENGTH) and 
						ADC_BUSY_r = '0' then
				-- This should happen after 5us of start.
					counter<= counter;
					if (ReadInConv = '1') or (next_state = idle) then 
						en_counter<= '0';
						
						CONVST(0) <= '0';
						CONVST(1) <= '0';								
					else 
						CONVST(0) <= '1';						--CONVSTA
						CONVST(1) <= '1';						--CONVSTB
					end if;
				end if;
				
			end if;			
		end if;	
	end process;
	

end Behavioral;



