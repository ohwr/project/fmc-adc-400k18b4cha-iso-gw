--------------------------------------------------------------------------------
-- CERN (BE-CO-HT)
-- FMC ADC 400ks/s 18bits core
-- http://www.ohwr.org/projects/fmc-adc-400k18b4cha
--------------------------------------------------------------------------------
--
-- unit name: sdb_meta_pkg (sdb_meta_pkg.vhd)
--
-- author: Matthieu Cattin (matthieu.cattin@cern.ch)
-- Modified by: Xevi Serra (xserra@cells.es)
--
-- date: 11-03-2013
--
-- description: Sdb meta-information for the FMC ADC 400ks/s 18b design for SPEC.
--
--------------------------------------------------------------------------------
-- GNU LESSER GENERAL PUBLIC LICENSE
--------------------------------------------------------------------------------
-- This source file is free software; you can redistribute it and/or modify it
-- under the terms of the GNU Lesser General Public License as published by the
-- Free Software Foundation; either version 2.1 of the License, or (at your
-- option) any later version. This source is distributed in the hope that it
-- will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
-- of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
-- See the GNU Lesser General Public License for more details. You should have
-- received a copy of the GNU Lesser General Public License along with this
-- source; if not, download it from http://www.gnu.org/licenses/lgpl-2.1.html
--------------------------------------------------------------------------------
-- last changes: see git log.
--------------------------------------------------------------------------------
-- TODO: - 
--------------------------------------------------------------------------------

library ieee;

use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.wishbone_pkg.all;

package sdb_meta_pkg is

  ------------------------------------------------------------------------------
  -- Meta-information sdb records
  ------------------------------------------------------------------------------

  -- Top module repository url
  constant c_repo_url_sdb : t_sdb_repo_url := (
    -- url (string, 63 char)
    repo_url => "https://gitcomputing.cells.es/electronics/em2.git              ");

  -- Synthesis informations
  constant c_synthesis_sdb : t_sdb_synthesis := (
    -- Top module name (string, 16 char)
    syn_module_name  => "spec_top_fmc_adc",
    -- Commit ID (hex string, 128-bit = 32 char)
    -- git log -1 --format="%H" | cut -c1-32
    syn_commit_id    => "697546304b7a1890aba8d6effd935a0f",
    -- Synthesis tool name (string, 8 char)
    syn_tool_name    => "ISE     ",
    -- Synthesis tool version (bcd encoded, 32-bit)
    syn_tool_version => x"00000133",
    -- Synthesis date (bcd encoded, 32-bit, yyyymmdd)
    syn_date         => x"20150714",
    -- Synthesised by (string, 15 char)
    syn_username     => "xserra         ");

  -- Integration record
  constant c_integration_sdb : t_sdb_integration := (
    product     => (
      vendor_id => x"000000000000a1ba",  -- CERN
      device_id => x"f0884024",          -- echo "spec_fmc-adc-400k18b4cha" | md5sum | cut -c1-8
      version   => x"00030000",          -- bcd encoded, [31:16] = major, [15:0] = minor
      date      => x"20140116",          -- yyyymmdd
      name      => "spec_fmcadc400k18b "));


end sdb_meta_pkg;


package body sdb_meta_pkg is
end sdb_meta_pkg;
