--------------------------------------------------------------------------------
-- CERN (BE-CO-HT)
-- FMC ADC mezzanine package
-- http://www.ohwr.org/projects/fmc-adc-100m14b4cha
--------------------------------------------------------------------------------
--
-- unit name: fmc_adc_mezzanine_pkg (fmc_adc_mezzanine_pkg.vhd)
--
-- author: Matthieu Cattin (matthieu.cattin@cern.ch)
--
-- date: 03-07-2013
--
-- version: 1.0
--
-- description: Package for FMC ADC mezzanine
--
-- dependencies:
--
--------------------------------------------------------------------------------
-- GNU LESSER GENERAL PUBLIC LICENSE
--------------------------------------------------------------------------------
-- This source file is free software; you can redistribute it and/or modify it
-- under the terms of the GNU Lesser General Public License as published by the
-- Free Software Foundation; either version 2.1 of the License, or (at your
-- option) any later version. This source is distributed in the hope that it
-- will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
-- of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
-- See the GNU Lesser General Public License for more details. You should have
-- received a copy of the GNU Lesser General Public License along with this
-- source; if not, download it from http://www.gnu.org/licenses/lgpl-2.1.html
--------------------------------------------------------------------------------
-- last changes: see svn log.
--------------------------------------------------------------------------------
-- TODO: - 
--------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;



package fmc_adc_mezzanine_pkg is

  ------------------------------------------------------------------------------
  -- Constants declaration
  ------------------------------------------------------------------------------


  ------------------------------------------------------------------------------
  -- Components declaration
  ------------------------------------------------------------------------------
  component fmc_adc_mezzanine
    generic(
      g_multishot_ram_size : natural := 2048;
      g_carrier_type       : string  := "SPEC"
      );
    port (
      -- Clock, reset
      sys_clk_i   : in std_logic;
      sys_rst_n_i : in std_logic;
		
		adc_clk_i   : in std_logic;

      -- CSR wishbone interface
      wb_csr_adr_i   : in  std_logic_vector(31 downto 0);
      wb_csr_dat_i   : in  std_logic_vector(31 downto 0);
      wb_csr_dat_o   : out std_logic_vector(31 downto 0);
      wb_csr_cyc_i   : in  std_logic;
      wb_csr_sel_i   : in  std_logic_vector(3 downto 0);
      wb_csr_stb_i   : in  std_logic;
      wb_csr_we_i    : in  std_logic;
      wb_csr_ack_o   : out std_logic;
      wb_csr_stall_o : out std_logic;

      

      -- FMC interface
		LA_P    : inout std_logic_vector(33 downto 0);
		LA_N    : inout std_logic_vector(33 downto 0);


	 
      sys_scl_b : inout std_logic;      -- Mezzanine system I2C clock (EEPROM)
      sys_sda_b : inout std_logic       -- Mezzanine system I2C data (EEPROM)
      );
  end component fmc_adc_mezzanine;

end fmc_adc_mezzanine_pkg;

package body fmc_adc_mezzanine_pkg is



end fmc_adc_mezzanine_pkg;
