--------------------------------------------------------------------------------
-- ALBA-CELLS (Computing Division, Electronics Section)
-- FMC ADC 400kSs/s core
-- http://www.ohwr.org/projects/fmc-adc-400k18b4cha
--------------------------------------------------------------------------------
--
-- unit name: FMC_ADC_400k_core (fmc_adc_18b_400ks_core.vhd)
--
-- author: Xevi Serra-Gallifa (xserra@cells.es)
--
-- date: 07-07-2015
--
-- description: FMC ADC 400ks/s 18bits core. 
--
-- dependencies:
--
-- references:
--    [1] fmc_adc_100Ms_core (fmc_adc_100Ms_core.vhd) by Matthieu Cattin (matthieu.cattin@cern.ch) and Theodor Stana (t.stana@cern.ch)
--
--------------------------------------------------------------------------------
-- GNU LESSER GENERAL PUBLIC LICENSE
--------------------------------------------------------------------------------
-- This source file is free software; you can redistribute it and/or modify it
-- under the terms of the GNU Lesser General Public License as published by the
-- Free Software Foundation; either version 2.1 of the License, or (at your
-- option) any later version. This source is distributed in the hope that it
-- will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
-- of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
-- See the GNU Lesser General Public License for more details. You should have
-- received a copy of the GNU Lesser General Public License along with this
-- source; if not, download it from http://www.gnu.org/licenses/lgpl-2.1.html
--------------------------------------------------------------------------------
-- last changes: see git log.
--------------------------------------------------------------------------------
-- TODO: - 
--------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;
use ieee.std_logic_unsigned.all;


library UNISIM;
use UNISIM.vcomponents.all;

library work;



entity FMC_ADC_400k_core is
  generic(
    g_carrier_type       : string  := "SPEC"
    );
  port (
    -- Clock, reset
    sys_clk_i   : in std_logic;
    sys_rst_n_i : in std_logic;
	 
	 adc_clk_i	 : in std_logic;

    -- CSR wishbone interface
    wb_csr_adr_i : in  std_logic_vector(3 downto 0);
    wb_csr_dat_i : in  std_logic_vector(31 downto 0);
    wb_csr_dat_o : out std_logic_vector(31 downto 0);
    wb_csr_cyc_i : in  std_logic;
    wb_csr_sel_i : in  std_logic_vector(3 downto 0);
    wb_csr_stb_i : in  std_logic;
    wb_csr_we_i  : in  std_logic;
    wb_csr_ack_o : out std_logic;


    -- FMC interface	 
	 adc_RESET			: out std_logic;
	 adc_nCS				: out std_logic;
	 adc_Data_OUT		: in STD_LOGIC_VECTOR ( 1 downto 0);
	 adc_SCLK			: out std_logic;
	 adc_ConvStart		: out STD_LOGIC_VECTOR ( 1 downto 0);
	 adc_BUSY			: in std_logic;
	 adc_OverSam		: out std_logic_vector(2 downto 0);
	 adc_Range			: out std_logic
	 
	 
    );
end FMC_ADC_400k_core;


architecture rtl of FMC_ADC_400k_core is


  ------------------------------------------------------------------------------
  -- Components declaration
  ------------------------------------------------------------------------------


component AD7608_CRTL 
    Generic( 
		CLK_To_5us : INTEGER := 100; 	-- Tconversion/ Tclk = 5us/60ns = 83clock cycles.												
		DISABLE_CLK : BOOLEAN := TRUE -- Disables Clock when is not needed.											
	 );
	 
	 Port ( n_reset : 		in  STD_LOGIC;
			  clk : 				in  STD_LOGIC;
           SCLK :				out STD_LOGIC;
			  Din : 				in  STD_LOGIC_VECTOR (1 downto 0);
           Trig : 			in  STD_LOGIC;
			  n_DblRateInput:	in  STD_LOGIC;
			  ReadInConv:		in  STD_LOGIC; --  becareful when 0 with DblRateInput
			  ADC_BUSY: 		in  STD_LOGIC;
			  ADC_RESET : 		out STD_LOGIC;
			  nCS : 				out STD_LOGIC;
			  CONVST: 			out STD_LOGIC_VECTOR ( 1 downto 0);
           ADC_OUT_A : 		out STD_LOGIC_VECTOR (17 downto 0);
			  ADC_OUT_B : 		out STD_LOGIC_VECTOR (17 downto 0);
           ADC_num : 		out STD_LOGIC_VECTOR (1 downto 0);
           DATA_READY : 	out STD_LOGIC;
			  FSMstate :      out STD_LOGIC_VECTOR ( 2 downto 0)
           );
end component AD7608_CRTL;

component fmc_adc_18b_400ks_csr is
  port (
    rst_n_i                                  : in     std_logic;
    clk_sys_i                                : in     std_logic;
    wb_adr_i                                 : in     std_logic_vector(3 downto 0);
    wb_dat_i                                 : in     std_logic_vector(31 downto 0);
    wb_dat_o                                 : out    std_logic_vector(31 downto 0);
    wb_cyc_i                                 : in     std_logic;
    wb_sel_i                                 : in     std_logic_vector(3 downto 0);
    wb_stb_i                                 : in     std_logic;
    wb_we_i                                  : in     std_logic;
    wb_ack_o                                 : out    std_logic;
    wb_stall_o                               : out    std_logic;
    adc_clk_i                                : in     std_logic;
-- Ports for PASS_THROUGH field: 'State machine commands (ignore on read)' in reg: 'Control register'
    fmc_adc_core_ctl_fsm_cmd_o               : out    std_logic_vector(1 downto 0);
-- Port for BIT field: 'Range' in reg: 'Control register'
    fmc_adc_core_ctl_adc_volt_rage_o         : out    std_logic;
-- Port for std_logic_vector field: 'Oversampling Mode' in reg: 'Control register'
    fmc_adc_core_ctl_adc_os_o                : out    std_logic_vector(2 downto 0);
-- Port for asynchronous (clock: adc_clk_i) BIT field: 'Double Rate Input' in reg: 'Control register'
    fmc_adc_core_ctl_adc_n_dbl_rate_o        : out    std_logic;
-- Port for std_logic_vector field: 'Reserved' in reg: 'Control register'
    fmc_adc_core_ctl_reserved_i              : in     std_logic_vector(24 downto 0);
-- Port for std_logic_vector field: 'State machine status' in reg: 'Status register'
    fmc_adc_core_sta_fsm_i                   : in     std_logic_vector(2 downto 0);
-- Port for asynchronous (clock: adc_clk_i) std_logic_vector field: 'ADC State machine status' in reg: 'Status register'
    fmc_adc_core_sta_adc_fsm_i               : in     std_logic_vector(2 downto 0);
-- Port for std_logic_vector field: 'Reserved' in reg: 'Status register'
    fmc_adc_core_sta_reserved_i              : in     std_logic_vector(25 downto 0);
-- Port for std_logic_vector field: 'ADC Chanel1' in reg: 'ADC Chanel 1'
    fmc_adc_core_adc_ch1_i                   : in     std_logic_vector(31 downto 0);
-- Port for std_logic_vector field: 'ADC Chanel2' in reg: 'ADC Chanel 2'
    fmc_adc_core_adc_ch2_i                   : in     std_logic_vector(31 downto 0);
-- Port for std_logic_vector field: 'ADC Chanel3' in reg: 'ADC Chanel 3'
    fmc_adc_core_adc_ch3_i                   : in     std_logic_vector(31 downto 0);
-- Port for std_logic_vector field: 'ADC Chanel4' in reg: 'ADC Chanel 4'
    fmc_adc_core_adc_ch4_i                   : in     std_logic_vector(31 downto 0);
-- Port for std_logic_vector field: 'ADC Chanel5' in reg: 'ADC Chanel 5'
    fmc_adc_core_adc_ch5_i                   : in     std_logic_vector(31 downto 0);
-- Port for std_logic_vector field: 'ADC Chanel6' in reg: 'ADC Chanel 6'
    fmc_adc_core_adc_ch6_i                   : in     std_logic_vector(31 downto 0);
-- Port for std_logic_vector field: 'ADC Chanel7' in reg: 'ADC Chanel 7'
    fmc_adc_core_adc_ch7_i                   : in     std_logic_vector(31 downto 0);
-- Port for std_logic_vector field: 'ADC Chanel8' in reg: 'ADC Chanel 8'
    fmc_adc_core_adc_ch8_i                   : in     std_logic_vector(31 downto 0);
-- Port for std_logic_vector field: 'Samples counter' in reg: 'Samples counter'
    fmc_adc_core_samples_cnt_i               : in     std_logic_vector(31 downto 0)
  );
end component fmc_adc_18b_400ks_csr;

  ------------------------------------------------------------------------------
  -- Constants declaration
  ------------------------------------------------------------------------------


  ------------------------------------------------------------------------------
  -- Types declaration
  ------------------------------------------------------------------------------
  type t_acq_fsm_state is (IDLE, PRE_TRIG, WAIT_TRIG, POST_TRIG, TRIG_TAG, DECR_SHOT);
  
  
  type adc_data_t is array ( 0 to 7) of std_logic_vector(31 downto 0);


  ------------------------------------------------------------------------------
  -- Signals declaration
  ------------------------------------------------------------------------------

signal fmc_adc_core_ctl_fsm_cmd_o: std_logic_vector(1 downto 0);

signal adc_data				: adc_data_t; 
signal adc_data_rdy  		: std_logic;
signal adc_chan		  		: std_logic_vector(1 downto 0);
signal adc_out_a		  		: std_logic_vector(17 downto 0);
signal adc_a_msb		  		: std_logic_vector(31 downto 18);
signal adc_out_b		  		: std_logic_vector(17 downto 0);
signal adc_b_msb		  		: std_logic_vector(31 downto 18);

signal adc_volt_rage       : std_logic;
signal adc_dbl_rate        : std_logic;

signal adc_core_sta_fsm		: std_logic_vector(2 downto 0);
signal trigger					: std_logic;

signal samples_cnt		  		: std_logic_vector(31 downto 0);


begin
		  
  ------------------------------------------------------------------------------
  -- Samples counter
  ------------------------------------------------------------------------------
  p_samples_cnt : process (sys_clk_i)
  begin
    if rising_edge(sys_clk_i) then
      if (adc_data_rdy = '1') then
        samples_cnt <= samples_cnt + 1;
      end if;
    end if;
  end process p_samples_cnt;

  ------------------------------------------------------------------------------
  -- Aqcuisition 
  ------------------------------------------------------------------------------
adc_a_msb<= (others=> adc_out_a(17));
adc_b_msb<= (others=> adc_out_b(17));


set_data_reg : process (sys_clk_i, adc_data_rdy)
  begin
    
    if rising_edge(sys_clk_i) then		
		if adc_data_rdy = '1' then			
			adc_data(TO_INTEGER('0' & UNSIGNED(adc_chan))) <= adc_a_msb & adc_out_a;
			adc_data(TO_INTEGER('1' & UNSIGNED(adc_chan))) <= adc_b_msb & adc_out_b;			
		end if;
	 end if;		
	 
  end process set_data_reg;

 
acq_ctrl : process (sys_clk_i)
	begin
    
    if rising_edge(sys_clk_i) then		
		if fmc_adc_core_ctl_fsm_cmd_o = "01" then 
			trigger<= '1';
		elsif fmc_adc_core_ctl_fsm_cmd_o = "10" then 
			trigger<= '0';
		end if;
	 end if;		
	 
  end process acq_ctrl;
  
  
  
 
	cmp_adc_AD7608: AD7608_CRTL 
	port map( 
		n_reset  		=> sys_rst_n_i,
		clk  				=> adc_clk_i,
		SCLK 				=> adc_SCLK,
		Din  				=> adc_Data_OUT,
      Trig  			=> trigger,
		n_DblRateInput => adc_dbl_rate,
		ReadInConv		=> '0', --  becareful when 0 with DblRateInput
		ADC_BUSY 		=> adc_BUSY,
		ADC_RESET  		=> adc_RESET,
		nCS 				=> adc_nCS,
		CONVST 			=> adc_ConvStart,
      ADC_OUT_A 		=> adc_out_a,
		ADC_OUT_B 		=> adc_out_b,
      ADC_num 			=> adc_chan,
      DATA_READY	 	=> adc_data_rdy,
		FSMstate		 	=> adc_core_sta_fsm
      );
		
		
  ------------------------------------------------------------------------------
  -- ADC core control and status registers (CSR)
  ------------------------------------------------------------------------------

cmp_fmc_adc_18b_400ks_csr : fmc_adc_18b_400ks_csr 
  port map(
    rst_n_i                                  => sys_rst_n_i,
    clk_sys_i                                => sys_clk_i,
    wb_adr_i                                 => wb_csr_adr_i,
    wb_dat_i                                 => wb_csr_dat_i,
    wb_dat_o                                 => wb_csr_dat_o,
    wb_cyc_i                                 => wb_csr_cyc_i,
    wb_sel_i                                 => wb_csr_sel_i,
    wb_stb_i                                 => wb_csr_stb_i,
    wb_we_i                	               => wb_csr_we_i,
    wb_ack_o                  	            => wb_csr_ack_o,
    wb_stall_o                   	         => open,
	 adc_clk_i                                => adc_clk_i,
	 
-- Ports for PASS_THROUGH field: 'State machine commands (ignore on read)' in reg: 'Control register'
    fmc_adc_core_ctl_fsm_cmd_o               => fmc_adc_core_ctl_fsm_cmd_o,
-- Port for BIT field: 'Range' in reg: 'Control register'
    fmc_adc_core_ctl_adc_volt_rage_o         => adc_volt_rage,
-- Port for std_logic_vector field: 'Oversampling Mode' in reg: 'Control register'
    fmc_adc_core_ctl_adc_os_o                => adc_OverSam,
-- Port for asynchronous (clock: adc_clk_i) BIT field: 'Double Rate Input' in reg: 'Control register'
    fmc_adc_core_ctl_adc_n_dbl_rate_o        => adc_dbl_rate,
-- Port for std_logic_vector field: 'Reserved' in reg: 'Control register'
    fmc_adc_core_ctl_reserved_i              => (others => '0'),
-- Port for std_logic_vector field: 'State machine status' in reg: 'Status register'
    fmc_adc_core_sta_fsm_i                   => "000",
-- Port for asynchronous (clock: adc_clk_i) std_logic_vector field: 'ADC State machine status' in reg: 'Status register'
    fmc_adc_core_sta_adc_fsm_i               => adc_core_sta_fsm,
-- Port for std_logic_vector field: 'Reserved' in reg: 'Status register'
    fmc_adc_core_sta_reserved_i              => (others => '1'),
-- Port for std_logic_vector field: 'ADC Chanel1' in reg: 'ADC Chanel 1'
    fmc_adc_core_adc_ch1_i                   => adc_data(0),
-- Port for std_logic_vector field: 'ADC Chanel2' in reg: 'ADC Chanel 2'
    fmc_adc_core_adc_ch2_i                   => adc_data(1),
-- Port for std_logic_vector field: 'ADC Chanel3' in reg: 'ADC Chanel 3'
    fmc_adc_core_adc_ch3_i                   => adc_data(2),
-- Port for std_logic_vector field: 'ADC Chanel4' in reg: 'ADC Chanel 4'
    fmc_adc_core_adc_ch4_i                   => adc_data(3),
-- Port for std_logic_vector field: 'ADC Chanel5' in reg: 'ADC Chanel 5'
    fmc_adc_core_adc_ch5_i                   => adc_data(4),
-- Port for std_logic_vector field: 'ADC Chanel6' in reg: 'ADC Chanel 6'
    fmc_adc_core_adc_ch6_i                   => adc_data(5),
-- Port for std_logic_vector field: 'ADC Chanel7' in reg: 'ADC Chanel 7'
    fmc_adc_core_adc_ch7_i                   => adc_data(6),
-- Port for std_logic_vector field: 'ADC Chanel8' in reg: 'ADC Chanel 8'
    fmc_adc_core_adc_ch8_i                   => adc_data(7),
-- Port for std_logic_vector field: 'Samples counter' in reg: 'Samples counter'
    fmc_adc_core_samples_cnt_i               => samples_cnt
		
  );
 

end rtl;
