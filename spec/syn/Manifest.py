target = "xilinx"
action = "synthesis"

syn_device = "xc6slx100t"
syn_grade = "-3"
syn_package = "fgg484"
syn_top = "spec_top_fmc_adc_400k18b4cha_iso"
syn_project = "spec_fmc_adc_400k18b4cha_iso.xise"

files = [
    "../spec_top_fmc_adc_400k18b4cha_iso.ucf",
    "../../ip_cores/multishot_dpram.ngc",
    "../../ip_cores/wb_ddr_fifo.ngc",
    "../../ip_cores/utils/utils_pkg.vhd"]

modules = { "local" : ["../rtl",
                       "../../adc/rtl"],
            "git" : ["git://ohwr.org/hdl-core-lib/general-cores.git::proposed_master",
                     "git://ohwr.org/hdl-core-lib/ddr3-sp6-core.git::spec_bank3_64b_32b",
                     "git://ohwr.org/hdl-core-lib/gn4124-core.git::master"]}

fetchto="../../ip_cores"
