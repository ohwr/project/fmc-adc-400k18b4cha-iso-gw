--------------------------------------------------------------------------------
-- ALBA-CELLS (Computing Division, Electronics Section)
-- FMC ADC 400kSs/s core
-- http://www.ohwr.org/projects/fmc-adc-400k18b4cha
--------------------------------------------------------------------------------
--
-- unit name: FV_CONTROL (FV_CONTROL.vhd)
--
-- author: Xevi Serra-Gallifa (xserra@cells.es)
--
-- date: 07-07-2015
--
-- description: FMC ADC 400ks/s 18bits core. 
--
-- dependencies:
--
-- references:
--    
--
--------------------------------------------------------------------------------
-- GNU LESSER GENERAL PUBLIC LICENSE
--------------------------------------------------------------------------------
-- This source file is free software; you can redistribute it and/or modify it
-- under the terms of the GNU Lesser General Public License as published by the
-- Free Software Foundation; either version 2.1 of the License, or (at your
-- option) any later version. This source is distributed in the hope that it
-- will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
-- of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
-- See the GNU Lesser General Public License for more details. You should have
-- received a copy of the GNU Lesser General Public License along with this
-- source; if not, download it from http://www.gnu.org/licenses/lgpl-2.1.html
--------------------------------------------------------------------------------
-- last changes: see git log.
--------------------------------------------------------------------------------
-- TODO: - 
--------------------------------------------------------------------------------
-- Additional Comments: 
--------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_arith.ALL;
use ieee.std_logic_unsigned.all;
use work.fmc_adc_18b_400ks_pkg.all;

entity FV_CONTROL is
	Generic( 
		ADC_bits 			: INTEGER := 10; 	-- (AD7910 --> 10, AD7920 --> 12)										
		CLk_ticks_to_SPI 	: INTEGER := 10										
	 );
	 Port ( 
		-- Clock, reset
		sys_clk_i   		: in std_logic;
		sys_rst_n_i 		: in std_logic;

		-- CSR wishbone interface
		wb_csr_adr_i 		: in  std_logic_vector(0 downto 0);
		wb_csr_dat_i 		: in  std_logic_vector(31 downto 0);
		wb_csr_dat_o 		: out std_logic_vector(31 downto 0);
		wb_csr_cyc_i 		: in  std_logic;
		wb_csr_sel_i 		: in  std_logic_vector(3 downto 0);
		wb_csr_stb_i 		: in  std_logic;
		wb_csr_we_i  		: in  std_logic;
		wb_csr_ack_o 		: out std_logic;


		-- FMC interface	 
		SCLK 					: out STD_LOGIC;
		SDATA 				: inout  STD_LOGIC; --Registered input, to avoid meta-inestabilities
		nCS 					: out STD_LOGIC;	
		HV_Led 				: out STD_LOGIC;
		SP1_Led 				: out STD_LOGIC;		
		HV_Relay 			: out STD_LOGIC
     );
end FV_CONTROL;

architecture Behavioral of FV_CONTROL is

  ------------------------------------------------------------------------------
  -- Components declaration
  ------------------------------------------------------------------------------
component fmc_adc_18b_FV_Ctrl_csr is
  port (
    rst_n_i                                  : in     std_logic;
    clk_sys_i                                : in     std_logic;
    wb_adr_i                                 : in     std_logic_vector(0 downto 0);
    wb_dat_i                                 : in     std_logic_vector(31 downto 0);
    wb_dat_o                                 : out    std_logic_vector(31 downto 0);
    wb_cyc_i                                 : in     std_logic;
    wb_sel_i                                 : in     std_logic_vector(3 downto 0);
    wb_stb_i                                 : in     std_logic;
    wb_we_i                                  : in     std_logic;
    wb_ack_o                                 : out    std_logic;
    wb_stall_o                               : out    std_logic;
-- Ports for BIT field: 'FV_relay_sta' in reg: 'Control and Status'
    fmc_fv_ctrl_ctrl_sta_relay_o             : out    std_logic;
    fmc_fv_ctrl_ctrl_sta_relay_i             : in     std_logic;
    fmc_fv_ctrl_ctrl_sta_relay_load_o        : out    std_logic;
-- Port for std_logic_vector field: 'FV_Led_sta' in reg: 'Control and Status'
    fmc_fv_ctrl_ctrl_sta_led_i               : in     std_logic_vector(2 downto 0);
-- Port for std_logic_vector field: 'Reading_mode' in reg: 'Control and Status'
    fmc_fv_ctrl_ctrl_sta_read_mode_o         : in     std_logic_vector(1 downto 0);
-- Port for BIT field: 'Spare Led' in reg: 'Control and Status'
    fmc_fv_ctrl_ctrl_sta_sp1_o               : out    std_logic;
-- Port for std_logic_vector field: 'Floating ground Voltage' in reg: 'Control and Status'
    fmc_fv_ctrl_ctrl_sta_fv_i                : in     std_logic_vector(9 downto 0);
-- Port for std_logic_vector field: 'Maximum Voltage' in reg: 'range of voltage voltages'
    fmc_fv_ctrl_lim_max_o                    : out    std_logic_vector(9 downto 0);
    fmc_fv_ctrl_lim_max_i                    : in     std_logic_vector(9 downto 0);
    fmc_fv_ctrl_lim_max_load_o               : out    std_logic;
-- Port for std_logic_vector field: 'Minimum voltage' in reg: 'range of voltage voltages'
    fmc_fv_ctrl_lim_min_o                    : out    std_logic_vector(9 downto 0);
    fmc_fv_ctrl_lim_min_i                    : in     std_logic_vector(9 downto 0);
    fmc_fv_ctrl_lim_min_load_o               : out    std_logic
  );
end component fmc_adc_18b_FV_Ctrl_csr;

  
component AD79X0_CRTL 
	Generic( 
		ADC_bits : 				INTEGER := 10; 	-- (AD7910 --> 10, AD7920 --> 12)										
		CLk_ticks_to_SPI :	INTEGER := 30										
	 );
	 Port ( reset : 			in  STD_LOGIC;
			  clk : 				in  STD_LOGIC;
           SCLK :				out STD_LOGIC;
			  SDATA : 			in  STD_LOGIC; --Registered input, to avoid meta-inestabilities
           nCS :	 			out STD_LOGIC;	
			  ADC_OUT : 		out STD_LOGIC_VECTOR (ADC_bits - 1 downto 0);
           DATA_READY : 	out STD_LOGIC
           );
end component AD79X0_CRTL;



		signal sta_relay_o	:	STD_LOGIC;
		signal sta_relay_i	:	STD_LOGIC;
		signal sta_relay_en	:	STD_LOGIC;
		-- Port for std_logic_vector field: 'Reading_mode' in reg: 'Control and Status'
		signal read_mode		:	STD_LOGIC_VECTOR (1 downto 0);
		-- Port for std_logic_vector field: 'Maximum Voltage' in reg: 'range of voltage voltages'
		signal FV_max_o 		:	STD_LOGIC_VECTOR (9 downto 0);
		signal FV_max_i 		:	STD_LOGIC_VECTOR (9 downto 0);
		signal FV_max_en 	:	STD_LOGIC;
		-- Port for std_logic_vector field: 'Minimum voltage' in reg: 'range of voltage voltages'
		signal FV_min_o 		:	STD_LOGIC_VECTOR (9 downto 0);
		signal FV_min_i 		:	STD_LOGIC_VECTOR (9 downto 0);
		signal FV_min_en 		:	STD_LOGIC;

		signal sys_rst 		:	STD_LOGIC;
		signal DATA_READY 	:	STD_LOGIC;
		signal ADC_OUT 		:	STD_LOGIC_VECTOR (ADC_bits - 1 downto 0);
		signal Led_cnt 		:	STD_LOGIC_VECTOR (27 downto 0);
		alias Led_pwm_cnt 	:	STD_LOGIC_VECTOR (17 downto 0) is Led_cnt(17 downto 0);
		signal LedHV_sta		:	STD_LOGIC_VECTOR (2 downto 0);
		signal LED_pwm			:	STD_LOGIC_VECTOR (1 downto 0);
		signal Led_on 			:	STD_LOGIC;
		signal HV_Relay_s		:	STD_LOGIC;


begin
	SDATA <= 'Z';
	sys_rst <= not sys_rst_n_i;
	
	process(sys_clk_i)
	begin
		if rising_edge(sys_clk_i) then 
			
			Led_cnt <= Led_cnt + 1;
			
			case LED_pwm is
				when "00" => HV_Led <= '0';				                                 									-- 0%
				when "01" => HV_Led <= Led_pwm_cnt(Led_pwm_cnt'high) and Led_pwm_cnt(Led_pwm_cnt'high - 1);			-- 25%
				when "10" => HV_Led <= Led_pwm_cnt(Led_pwm_cnt'high);																-- 50%
				when others => HV_Led <= '1';																								-- 100%
			end case;
		end if;
	end process;

	process(sys_clk_i)
	begin
		if rising_edge(sys_clk_i) then 		
			
			if LedHV_sta= "111" then
				if (Led_cnt(Led_cnt'high - 1)) = '1' then
					LED_pwm <= "11";
				else
					LED_pwm <= "10";
				end if;
			elsif LedHV_sta= "011" then
				if Led_cnt(Led_cnt'high-1) = '1' then
					LED_pwm <= "11";
				else
					LED_pwm <= "00";
				end if;
			elsif LedHV_sta= "001" then
				if (Led_cnt(Led_cnt'high) ) = '1' then
					LED_pwm <= "10";
				else
					LED_pwm <= "00";
				end if;
			elsif LedHV_sta= "000" then
				if (Led_cnt(Led_cnt'high) and Led_cnt(Led_cnt'high - 1) 
				    and Led_cnt(Led_cnt'high - 2) and Led_cnt(Led_cnt'high - 3)) = '1' then
					LED_pwm <= "01";
				else
					LED_pwm <= "00";
				end if;
			else
				LED_pwm <= "00";
			end if;
		end if;
	end process;


	process(sys_clk_i)
	begin
		if rising_edge(sys_clk_i) then 
			--if DATA_READY = '1' then
				if ADC_OUT >= FV_max_i then  -- Voltage out of maximum FV allowed
					LedHV_sta <= "111";
				elsif ADC_OUT <= FV_min_i then  -- Voltage out of minimum FV allowed
					LedHV_sta <= "111";
				elsif ADC_OUT > volt_to_adc(+40) then  -- FV Voltage is concidered HV
					LedHV_sta <= "011";
				elsif ADC_OUT < volt_to_adc(-40) then  -- FV Voltage is concidered HV
					LedHV_sta <= "011";
				elsif ADC_OUT > volt_to_adc(+10) then  -- Bias aplied in a safety range
					LedHV_sta <= "001";
				elsif ADC_OUT < volt_to_adc(-10) then  -- Bias aplied in a safety range
					LedHV_sta <= "001";
				else  -- Safe state
					LedHV_sta <= "000";
				end if;
			--end if;
		end if;
	end process;


	
	process(sys_clk_i)
	begin
		if rising_edge(sys_clk_i) then 
			if sys_rst_n_i = '0' then
				FV_max_i <= volt_to_adc(+20);
				FV_min_i <= volt_to_adc(-20);
				HV_Relay_s <= '0';
			else
				if FV_min_en = '1' then
					if FV_min_o > volt_to_adc(MinVolt) then
						FV_min_i <= FV_min_o;
					else 
						FV_min_i <= volt_to_adc(MinVolt);
					end if;
				end if;
				if FV_max_en= '1' then
					if FV_max_o < volt_to_adc(MaxVolt) then
						FV_max_i <= FV_max_o;
					else 
						FV_max_i <= volt_to_adc(MaxVolt);
					end if;
				end if;
				
				if (ADC_OUT >= FV_max_i) OR (ADC_OUT <= FV_min_o) then
					HV_Relay_s <= '0';
				elsif sta_relay_en= '1' then
					HV_Relay_s <= sta_relay_i;
				end if;				
			end if;
		end if;
	end process;
	
	sta_relay_i <= HV_Relay_s;
	HV_Relay <= HV_Relay_s;


	cmp_adc_AD79X0: AD79X0_CRTL 	
	 Port map(
			reset 		=> sys_rst,
			clk 			=> sys_clk_i,
			SCLK 			=> SCLK,
			SDATA 		=> SDATA,
			nCS 			=> nCS,
			ADC_OUT 		=> ADC_OUT,
			DATA_READY 	=> DATA_READY
         );

cmp_FV_Ctrl_csr : fmc_adc_18b_FV_Ctrl_csr
  port map (
    rst_n_i                                  => sys_rst_n_i,
    clk_sys_i                                => sys_clk_i,
    wb_adr_i                                 => wb_csr_adr_i,
    wb_dat_i                                 => wb_csr_dat_i,
    wb_dat_o                                 => wb_csr_dat_o,
    wb_cyc_i                                 => wb_csr_cyc_i,
    wb_sel_i                                 => wb_csr_sel_i,
    wb_stb_i                                 => wb_csr_stb_i,
    wb_we_i                	               => wb_csr_we_i,
    wb_ack_o                  	            => wb_csr_ack_o,
    wb_stall_o                   	         => open,

-- Ports for BIT field: 'FV_relay_sta' in reg: 'Control and Status'
    fmc_fv_ctrl_ctrl_sta_relay_o             => sta_relay_o,
    fmc_fv_ctrl_ctrl_sta_relay_i             => sta_relay_i,
    fmc_fv_ctrl_ctrl_sta_relay_load_o        => sta_relay_en,
-- Port for std_logic_vector field: 'FV_Led_sta' in reg: 'Control and Status'
    fmc_fv_ctrl_ctrl_sta_led_i               => LedHV_sta,
-- Port for std_logic_vector field: 'Reading_mode' in reg: 'Control and Status'
    fmc_fv_ctrl_ctrl_sta_read_mode_o         => read_mode,
	 fmc_fv_ctrl_ctrl_sta_sp1_o			      => SP1_led,
-- Port for std_logic_vector field: 'Floating ground Voltage' in reg: 'Control and Status'
    fmc_fv_ctrl_ctrl_sta_fv_i                => ADC_OUT,
-- Port for std_logic_vector field: 'Maximum Voltage' in reg: 'range of voltage voltages'
    fmc_fv_ctrl_lim_max_o                    => FV_max_o,
    fmc_fv_ctrl_lim_max_i                    => FV_max_i,
    fmc_fv_ctrl_lim_max_load_o               => FV_max_en,
-- Port for std_logic_vector field: 'Minimum voltage' in reg: 'range of voltage voltages'
    fmc_fv_ctrl_lim_min_o                    => FV_min_o,
    fmc_fv_ctrl_lim_min_i                    => FV_min_i,
    fmc_fv_ctrl_lim_min_load_o               => FV_min_en
  );

	
end Behavioral;